# About #

This is a public location for posting known issues about the SAE AS-4, IOP, and AEODRS JAUS services and documents. The intended audience is any developer of a system that communicates using JAUS.  Ownership of the relevant documents is assumed.

This is run on a volunteer basis and so issues/questions will be addressed when they can be but are not guaranteed. However, any issues that are posted will be seen by members of the SAE JAUS Working Group, which increases the likelihood of them being resolved in future document releases.

There are three main intents for this repository:

 1.      Record known issues with the standard that have agreed upon fixes but haven't made it into the standard as yet.
 2.      Log issues that don't have an agreed upon fix so that they can be discussed online and/or brought up at a Working Group meeting.
 3.      Ask questions about the standard or how a service is supposed to be used.
 
## Submitting Guidelines
 1.      Tag the issue 
 2.      Due to copyright, the full JSIDLs cannot be shared here. If necessary, include only the relevant portion of JSIDL files.  
 
## SAE AS-4 JAUS 
SAE (Society of Automotive Engineers) is the standards body responsible for JAUS.  AS-4 is the working group that develops JAUS. The SAE AS-4 JAUS standards are the active standards and supersede previous version.
 
## IOP (Interoperability Profile)
The IOP is a set of guidelines for how to design or spec a robotics system. JAUS is used as the communications framework, as such there are some custom JAUS messages developed under IOP that are not a part of the standard SAE AS-4 JAUS.
 
## AEODRS (Advanced EOD Robotics System)
AEODRS is a specific robotics system that uses JAUS. Some AEODRS designs and JAUS messages are incorporated into the IOP.
 
